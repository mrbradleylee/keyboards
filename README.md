# Keyboard Layouts

Current keyboards:

- Keebio Iris Rev 6 (daily driver)
- OLKB Preonic Rev 3
- YMDK Wings HotSwap

Keymaps created in VIA, requires flashing with VIA enabled firmware.

## Macros

- 1Pass: `{KC_LGUI,KC_LSFT,KC_X}`
- Mission Control: `{KC_LALT,KC_LCTL,KC_LEFT}`
  - add `KC_RIGHT` and `KC_UP` variations
- Chrome Tabs: `{KC_LGUI,KC_LALT,KC_LEFT}`
  - add `KC_RIGHT` as well

## Mapping `Hyper` key in VIA

The `Hyper` modifier is not recognized in VIA via the `KC_HYPR` keycode.
Instead, use: `MT(MOD_HYPR,KC_NO)`
